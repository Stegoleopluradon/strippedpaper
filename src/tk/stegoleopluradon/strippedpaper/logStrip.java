package tk.stegoleopluradon.strippedpaper;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class logStrip implements Listener {
	ArrayList<Material> allowedLogs;
	ArrayList<Material> allowedMaterials;

	public logStrip(ArrayList<Material> al, ArrayList<Material> am) {
		this.allowedLogs = al;
		this.allowedMaterials = am;
	}

	@EventHandler
	public void onLogStrip(PlayerInteractEvent e) {

		try {
			Material block = e.getClickedBlock().getType();
			if ((allowedLogs.contains(block))
					&& (allowedMaterials.contains(e.getPlayer().getEquipment().getItemInMainHand().getType()))) {
				Player player = e.getPlayer();
				Location location = e.getClickedBlock().getLocation();
				int rand = new Random().nextInt(10);
				if (rand <= 2)
					player.getWorld().dropItem(location, new ItemStack(Material.PAPER, 1));
			}

		} catch (NullPointerException ex) {
		}
	}

}
