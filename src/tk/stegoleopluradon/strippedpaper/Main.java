package tk.stegoleopluradon.strippedpaper;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	ArrayList<Material> allowedMaterials = new ArrayList<>();
	ArrayList<Material> allowedLogs = new ArrayList<>();

	public void setup() {
		allowedLogs.add(Material.ACACIA_LOG);
		allowedLogs.add(Material.BIRCH_LOG);
		allowedLogs.add(Material.SPRUCE_LOG);
		allowedLogs.add(Material.OAK_LOG);
		allowedLogs.add(Material.DARK_OAK_LOG);
		allowedLogs.add(Material.JUNGLE_LOG);
		allowedMaterials.add(Material.GOLDEN_AXE);
		allowedMaterials.add(Material.DIAMOND_AXE);
		allowedMaterials.add(Material.IRON_AXE);
		allowedMaterials.add(Material.STONE_AXE);
		allowedMaterials.add(Material.WOODEN_AXE);
	}

	@Override
	public void onEnable() {
		setup();
		Bukkit.getPluginManager().registerEvents(new logStrip(allowedLogs, allowedMaterials), this);
	}

	@Override
	public void onDisable() {
	}

}
